package E4_SemBin2;

import static Utils.UtilesHilo.nombre;

/**
 *
 * @author Alejandro Younes
 */
public class Avion implements Runnable {

    public enum Accion {
        ATERRIZAR("ATERRIZAR"),
        DESPEGAR("DESPEGAR");

        private final String valor;

        private Accion(String valor) {
            this.valor = valor;
        }
    }

    private Accion accion;
    private final Pista pista;
    private final int numero;

    public Avion(Accion accion, Pista pista, int numero) {
        this.accion = accion;
        this.pista = pista;
        this.numero = numero;
    }

    @Override
    public void run() {
        final int VECES = 5;
        System.out.println(nombre() + ": inicia");
        for (int i = 0; i < VECES; i++) {
            System.out.println(nombre() + ": quiere " + accion.valor);
            if (accion.equals(Accion.ATERRIZAR)) {
                pista.solicitarAterrizaje();
                pista.ocuparPista(accion);
                accion = Accion.DESPEGAR;
            } else {
                pista.solicitarDespegue();
                pista.ocuparPista(accion);
                accion = Accion.ATERRIZAR;
            }
        }
        System.out.println(nombre() + ": termina");
    }
}
