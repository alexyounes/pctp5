package E4_SemBin2;

import E4_SemBin2.Avion.Accion;
import static Utils.UtilesHilo.nombre;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author Alejandro Younes
 */
public class Pista {

    private final Semaphore semDespegues = new Semaphore(0);
    private final ReentrantLock mutexPista = new ReentrantLock();
    private final ReentrantLock mutexAterrizaje = new ReentrantLock();
    private final ReentrantLock mutexDespegue = new ReentrantLock();
    private final ReentrantLock mutexAterrizados = new ReentrantLock();
    private final ReentrantLock mutexPrioritario = new ReentrantLock();
    private int queriendoAterrizaje = 0;
    private int queriendoDespegue = 0;
    private int contadorAterrizados = 0;
    private int despeguePrioritario = 0;

    public void solicitarAterrizaje() {
        mutexAterrizaje.lock();
        queriendoAterrizaje++;
        mutexAterrizaje.unlock();
        mutexDespegue.lock();
        if (queriendoDespegue > 0) {
            mutexPrioritario.lock();
            if (despeguePrioritario > 0) {
                semDespegues.release();
                despeguePrioritario--;
            }
            mutexPrioritario.unlock();
        }
        mutexDespegue.unlock();
        System.out.println(nombre() + ": recibe permiso ATERRIZAJE");
    }

    public void solicitarDespegue() {
        mutexDespegue.lock();
        queriendoDespegue++;
        mutexDespegue.unlock();
        mutexAterrizaje.lock();
        if (queriendoAterrizaje == 0) {
            semDespegues.release();
        }
        mutexAterrizaje.unlock();
        try {
            semDespegues.acquire();
        } catch (InterruptedException ex) {
        }
        System.out.println(nombre() + ": recibe permiso DESPEGUE");
    }

    public void ocuparPista(Accion accion) {
        System.out.println(nombre() + ": quiere ocupar la pista");
        mutexPista.lock();
        System.out.println(nombre() + ": ocupa y desocupa PISTA");
//        simularActividad(2000, "usar la pista para " + accion);
        mutexPista.unlock();
        if (accion.equals(Accion.ATERRIZAR)) {
            mutexAterrizaje.lock();
            queriendoAterrizaje--;
            if (queriendoAterrizaje == 0 && queriendoDespegue > 0) {
                semDespegues.release();
            }
            mutexAterrizaje.unlock();
            mutexAterrizados.lock();
            contadorAterrizados++;
            mutexAterrizados.unlock();
            if (contadorAterrizados % 10 == 0) {
                mutexPrioritario.lock();
                despeguePrioritario++;
                mutexPrioritario.unlock();
            }
        } else {
            mutexDespegue.lock();
            queriendoDespegue--;
            if (queriendoAterrizaje == 0 && queriendoDespegue > 0) {
                semDespegues.release();
            }
            mutexDespegue.unlock();
        }
        System.out.println(nombre() + ": " + queriendoAterrizaje + " queriendo Aterrizaje");
    }
}
