package E2;

/**
 *
 * @author Alejandro Younes
 */
public class Main {

    public static void main(String[] args) {
    }
}

// 2. La protectora de Animales se dió cuenta que los perros comen más cantidad de alimento
// que los gatos, por lo que, como solución a su problema proponen darle 2 raciones de
// alimento a los perros (es decir, que cada perro ocupa 2 comederos) y los gatos siguen con
// una sola ración. Encuentre una solución óptima al nuevo problema planteado.
// Observación: ¿Qué sucede cuando la cantidad de comederos es impar? Como
// solucionaria ese problema sin agregar o quitar comederos.
