package E4_SemGen;

import static Utils.UtilesHilo.nombre;
import static Utils.UtilesImprimir.escribirLineaColor;

/**
 *
 * @author Alejandro Younes
 */
public class Avion implements Runnable {

    public enum Accion {
        ATERRIZAR("ATERRIZAR"),
        DESPEGAR("DESPEGAR");

        private final String valor;

        private Accion(String valor) {
            this.valor = valor;
        }
    }

    private Accion accion;
    private final Pista pista;
    private final int numero;

    public Avion(Accion accion, Pista pista, int numero) {
        this.accion = accion;
        this.pista = pista;
        this.numero = numero;
    }

    @Override
    public void run() {
        final int VECES = 5;
        escribirLineaColor(nombre() + ": inicia");
        for (int i = 0; i < VECES; i++) {
            escribirLineaColor(nombre() + ": quiere " + accion.valor);
            try {
                if (accion.equals(Accion.ATERRIZAR)) {
                    pista.solicitarAterrizaje();
                    pista.ocuparPista(accion);
                    accion = Accion.DESPEGAR;
                } else {
                    pista.solicitarDespegue();
                    pista.ocuparPista(accion);
                    accion = Accion.ATERRIZAR;
                }
            } catch (InterruptedException ex) {
            }
        }
        escribirLineaColor(nombre() + ": termina");
    }
}
