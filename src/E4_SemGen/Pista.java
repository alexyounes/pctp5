package E4_SemGen;

import E4_SemGen.Avion.Accion;
import static Utils.UtilesHilo.nombre;
import static Utils.UtilesImprimir.escribirLineaColor;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author Alejandro Younes
 */
public class Pista {

  private final Semaphore semDespegues = new Semaphore(0);
  private final ReentrantLock mutexPista = new ReentrantLock();
  private final ReentrantLock mutexAterrizados = new ReentrantLock();
  private int contadorAterrizados = 0;
  private final Semaphore semQuiereAterrizar = new Semaphore(0);
  private final Semaphore semQuiereDespegar = new Semaphore(0);
  private final Semaphore semPrioritario = new Semaphore(0);

  public void solicitarAterrizaje() {
    semQuiereAterrizar.release(); // Libera un permiso para indicar que quiere aterrizar
    if (semQuiereDespegar.tryAcquire()) { // Pregunta si hay aviones queriendo despegar
      if (semPrioritario.tryAcquire()) { // Pregunta si hay un permiso de vuelo prioritario
        semDespegues.release(); // Permite a un avion despegar
      }
      semQuiereDespegar.release(); // Si hay despegues libera el permiso consumido al preguntar
    }
    escribirLineaColor(nombre() + ": recibe permiso ATERRIZAJE");
  }

  public void solicitarDespegue() throws InterruptedException {
    semQuiereDespegar.release(); // Libera un permiso para indicar que quiere despegar
    if (!semQuiereAterrizar.tryAcquire()) { // Pregunta si no hay aterrizajes
      semDespegues.release(); // Permite a un avion despegar
    } else {
      semQuiereAterrizar.release(); // Si hay aterrizajes libera el permiso consumido al preguntar
    }
    semDespegues.acquire(); // Espera recibir permiso para despegar
    escribirLineaColor(nombre() + ": recibe permiso DESPEGUE");
  }

  public void ocuparPista(Accion accion) throws InterruptedException {
    escribirLineaColor(nombre() + ": quiere ocupar la pista");
    mutexPista.lock(); // No permite a otros utilizar la pista
    escribirLineaColor(nombre() + ": ocupa y desocupa PISTA");
    mutexPista.unlock(); // Permite a otros utilizar la pista
    if (accion.equals(Accion.ATERRIZAR)) {
      semQuiereAterrizar.acquire(); // Consume un permiso para indicar que ya aterrizo
      liberarDespegueSiNoHayAterrizajes();
      mutexAterrizados.lock(); // No permite a otros mas utilizar contador de aterrizados
      contadorAterrizados++;
      if (contadorAterrizados % 10 == 0) { // Si aterrizaron 10 mas
        semPrioritario.release(); // Suma un permiso de despegue prioritario
      }
      mutexAterrizados.unlock(); // Permite a otros utilizar contador de aterrizados
    } else {
      semQuiereDespegar.acquire(); // Consume un permiso para indicar que ya despego
      liberarDespegueSiNoHayAterrizajes();
    }
  }

  private void liberarDespegueSiNoHayAterrizajes() {
    if (!semQuiereAterrizar.tryAcquire()) { // Pregunta si no hay aterrizajes
      if (semQuiereDespegar.tryAcquire()) { // Pregunta si hay aviones queriendo despegar
        semDespegues.release(); // Permite a un avion despegar
        semQuiereDespegar.release(); // Libera el permiso consumido al preguntar
      }
    } else {
      semQuiereAterrizar.release(); // Si hay aterrizajes libera el permiso consumido al preguntar
    }
  }
}
