package E3_SemGen;

/**
 *
 * @author Alejandro Younes
 */
public class Control implements Runnable {

    private final Tren tren;

    public Control(Tren tren) {
        this.tren = tren;
    }

    @Override
    public void run() {
        while (true) {
            tren.hacerCircuito();
        }
    }
}
