package E3_SemGen;

/**
 *
 * @author Alejandro Younes
 */
public class Pasajero implements Runnable {

    private final Tren tren;

    public Pasajero(Tren tren) {
        this.tren = tren;
    }

    @Override
    public void run() {
        tren.comprarTicket();
        tren.subirTren();
        tren.bajarTren();
    }

}
