package E3_SemGen;

import static Utils.UtilesHilo.nombre;
import static Utils.UtilesHilo.simularActividad;
import java.util.concurrent.Semaphore;

/**
 *
 * @author Alejandro Younes
 */
public class Tren {

    private final int c;
    private int ocupados = 0;

    private final Semaphore semTren = new Semaphore(0);
    private final Semaphore semTicket = new Semaphore(0);
    private final Semaphore semSubir = new Semaphore(0);
    private final Semaphore semBajar = new Semaphore(0);

    public Tren(int c) {
        this.c = c;
    }

    public void hacerCircuito() {
        System.out.println("Tren espera para arrancar");
        semSubir.release(c);
        try {
            semTren.acquire();
        } catch (InterruptedException ex) {
        }
        simularActividad(5000, "dar una vuelta (5s)");
        System.out.println("Tren vuelve a punto de partida, avisa a pasajeros");
        semBajar.release(c);
    }

    public void venderTicket() {
        System.out.println(nombre() + ": espera pasajero");
        try {
            semTicket.acquire();
        } catch (InterruptedException ex) {
        }
        System.out.println(nombre() + ": vende ticket");
    }

    public void comprarTicket() {
        System.out.println(nombre() + ": compra ticket");
        semTicket.release();
    }

    public void subirTren() {
        System.out.println(nombre() + ": hace cola para subir");
        try {
            semSubir.acquire();
        } catch (InterruptedException ex) {
        }
        System.out.println(nombre() + ": sube al tren");
        ocupados++;
        if (ocupados == c) {
            System.out.println(nombre() + ": tren lleno, avisa a maquinista");
            semTren.release();
        }
    }

    public void bajarTren() {
        System.out.println(nombre() + ": espera para bajar");
        try {
            semBajar.acquire();
        } catch (InterruptedException ex) {
        }
        ocupados--;
        System.out.println(nombre() + ": baja del tren");
    }
}
