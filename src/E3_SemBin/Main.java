package E3_SemBin;

/**
 *
 * @author Alejandro Younes
 */
public class Main {

    public static void main(String[] args) {
        int c = 5;
        int n = 20;
        int i;
        Tren tren = new Tren(c);
        Vendedor vendedor = new Vendedor(tren);
        Thread hiloVendedor = new Thread(vendedor, "Vendedor");
        hiloVendedor.start();
        Control control = new Control(tren);
        Thread hiloControl = new Thread(control, "Control");
        hiloControl.start();
        Pasajero[] pasajero = new Pasajero[n];
        Thread[] hiloPasajero = new Thread[n];
        for (i = 0; i < n; i++) {
            pasajero[i] = new Pasajero(tren);
            hiloPasajero[i] = new Thread(pasajero[i], "Pasajero " + i);
            hiloPasajero[i].start();
        }
    }
}

// 3. Tren turístico
// En un lugar de recreación para la familia, hay una atracción que es el
// “Tren Turístico” que hace un recorrido por todo el lugar. El tren tiene espacio
// para C pasajeros. Los pasajeros repetidamente esperan para tomar un lugar en el
// tren. El tren sólo puede salir a hacer su recorrido si está lleno.
// ● El tren no tiene paradas intermedias.
// ● Ningún pasajero puede bajarse del tren en medio del recorrido.
// ● Ningún pasajero puede subir al tren en medio del recorrido
// ● Ningún pasajero puede dar otra vuelta antes de bajar del tren.
// ● Los pasajeros deben comprar un ticket para poder utilizar la atracción.
// ● Considere que el tren siempre logra llenarse.
// ● Los pasajeros que no pueden subir porque está completo deben esperar hasta la
// próxima vuelta.
// a. Considere un hilo vendedorTickets​ , un hilo controlTren​ y N hilos
// pasajero (N>C).
