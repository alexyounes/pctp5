package E3_SemBin;

/**
 *
 * @author Alejandro Younes
 */
public class Vendedor implements Runnable {

    private final Tren tren;

    public Vendedor(Tren tren) {
        this.tren = tren;
    }

    @Override
    public void run() {
        while (true) {
            tren.venderTicket();
        }
    }

}
