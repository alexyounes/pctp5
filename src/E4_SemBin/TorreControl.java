package E4_SemBin;

import static Utils.UtilesHilo.nombre;

/**
 *
 * @author Alejandro Younes
 */
public class TorreControl implements Runnable {

    private final Pista pista;

    public TorreControl(Pista pista) {
        this.pista = pista;
    }

    @Override
    public void run() {
        while (true) {
            pista.esperarAviones();
            boolean esAterrizaje = pista.preguntarSiEsAterrizaje();
            boolean hayAvionesParaAterrizar = pista.preguntarSiHayAvionesQueriendoAterrizar();
            int cantidadAterrizajes = pista.preguntarCantidadAterrizajes();
            if (cantidadAterrizajes >= 10 && !esAterrizaje) {
                System.out.println(nombre() + ": hubo 10 o más aterrizajes, autoriza despegue");
                pista.reiniciarContadorAterrizajes();
                pista.responderPedido("autoriza");
            } else if (esAterrizaje) {
                System.out.println(nombre() + ": es aterrizaje, autorizo");
                pista.responderPedido("autoriza");
            } else if (hayAvionesParaAterrizar) {
                System.out.println(nombre() + ": hay aviones sin aterrizar, deniego despegue");
                pista.responderPedido("deniega");
            } else {
                System.out.println(nombre() + ": es despegue y no hay aterrizajes, autorizo");
                pista.responderPedido("autoriza");
            }
        }
    }
}
