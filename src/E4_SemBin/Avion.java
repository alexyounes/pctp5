package E4_SemBin;

import static Utils.UtilesHilo.nombre;
import static Utils.UtilesHilo.simularActividad;

/**
 *
 * @author Alejandro Younes
 */
public class Avion implements Runnable {

    private boolean esAterrizaje;
    private final Pista pista;
    private final int numero;

    public Avion(boolean esAterrizaje, Pista pista, int numero) {
        this.esAterrizaje = esAterrizaje;
        this.pista = pista;
        this.numero = numero;
    }

    public boolean accionAvion(String accion) {
        pista.iniciarPedido(accion, numero);
        pista.avisarTorre();
        System.out.println(nombre() + ": espera autorizacion de torre");
        boolean autorizacion = pista.preguntarSiFueAutorizado();
        pista.finalizarPedido();
        if (autorizacion) {
            System.out.println(nombre() + ": se le PERMITIO " + accion);
            pista.ocuparPista();
            simularActividad(1000, accion);
            pista.desocuparPista(esAterrizaje);
        } else {
            System.out.println(nombre() + ": se le DENEGO " + accion);
            simularActividad(2000, "esperar (2 seg)");
        }
        return autorizacion;
    }

    @Override
    public void run() {
        int veces = 5;
        System.out.println(nombre() + ": arranca sus motores (INICIO)");
        for (int i = 0; i < veces; i++) {
            if (esAterrizaje) {
                accionAvion("aterrizar");
                simularActividad(2000, "quedar en tierra (2 seg)");
                esAterrizaje = false;
            } else {
                boolean pudoDespegar;
                do {
                    pudoDespegar = accionAvion("despegar");
                    if (pudoDespegar) {
                        simularActividad(2000, "volar (2 seg)");
                        esAterrizaje = true;
                    }
                } while (!pudoDespegar);
            }
        }
        System.out.println(nombre() + ": apaga sus motores (FIN)");
    }
}
