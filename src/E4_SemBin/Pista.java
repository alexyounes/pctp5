package E4_SemBin;

import static Utils.UtilesHilo.nombre;
import java.util.concurrent.Semaphore;

/**
 *
 * @author Alejandro Younes
 */
public class Pista {

    private final Semaphore semAvion = new Semaphore(0, true);
    private final Semaphore semTorre = new Semaphore(0, true);
    private final Semaphore semPista = new Semaphore(1, true);
    private final Semaphore semMutexPedido = new Semaphore(1, true);
    private final Semaphore semMutexAterrizaje = new Semaphore(1, true);
    private boolean permitido = false;
    private int avionesQueriendoAterrizar = 0;
    private boolean avionPedidoEsAterrizaje;
    private int numAvion;
    private int cantidadAterrizajes = 0;

    public void iniciarPedido(String accion, int numero) {
        try {
            semMutexPedido.acquire();
            System.out.println(nombre() + ": quiere " + accion);
            numAvion = numero;
        } catch (InterruptedException ex) {
            System.out.println("Excepcion al adquirir semMutexPedido");
        }
        if (accion.equals("aterrizar")) {
            avionPedidoEsAterrizaje = true;
            try {
                semMutexAterrizaje.acquire();
            } catch (InterruptedException ex) {
                System.out.println("Excepcion al adquirir semMutexAterrizaje");
            }
            avionesQueriendoAterrizar++;
            semMutexAterrizaje.release();
        } else {
            avionPedidoEsAterrizaje = false;
        }
    }

    public int preguntarCantidadAterrizajes() {
        return cantidadAterrizajes;
    }

    public boolean preguntarSiEsAterrizaje() {
        return avionPedidoEsAterrizaje;
    }

    public boolean preguntarSiHayAvionesQueriendoAterrizar() {
        boolean queriendoAterrizar = false;
        System.out.println("(DEBUG) Aterrizando: " + avionesQueriendoAterrizar);
        if (avionesQueriendoAterrizar > 0) {
            queriendoAterrizar = true;
        }
        return queriendoAterrizar;
    }

    public boolean preguntarSiFueAutorizado() {
        try {
            semAvion.acquire();
        } catch (InterruptedException ex) {
        }
        return permitido;
    }

    public void ocuparPista() {
        System.out.println(nombre() + ": espera para ocupar la pista");
        try {
            semPista.acquire();
        } catch (InterruptedException ex) {
            System.out.println("Excepcion al adquirir semPista");
        }
        System.out.println(nombre() + ": esta ocupando la pista");
    }

    public void desocuparPista(boolean avionPedidoEsAterrizaje) {
        if (avionPedidoEsAterrizaje) {
            try {
                semMutexAterrizaje.acquire();
            } catch (InterruptedException ex) {
                System.out.println("Excepcion al adquirir semMutexAterrizaje");
            }
            avionesQueriendoAterrizar--;
            cantidadAterrizajes++;
            System.out.println("(DEBUG) Cantidad Aterrizajes: " + cantidadAterrizajes);
            semMutexAterrizaje.release();
        }
        System.out.println(nombre() + ": desocupo la pista");
        semPista.release();
    }

    public void responderPedido(String respuesta) {
        System.out.println(nombre() + ":" + respuesta + " accion al avion " + numAvion);
        if (respuesta.equals("autoriza")) {
            permitido = true;
        } else {
            permitido = false;
        }
        semAvion.release();
    }

    public void finalizarPedido() {
        System.out.println(nombre() + ": finalizo su pedido");
        semMutexPedido.release();
    }

    public void esperarAviones() {
        System.out.println(nombre() + ": no hay aviones, espera");
        try {
            semTorre.acquire();
        } catch (InterruptedException ex) {
            System.out.println("Excepcion al adquirir semTorre");
        }
    }

    public void avisarTorre() {
        System.out.println(nombre() + ": quiere pedir permiso");
        semTorre.release();
    }

    public void reiniciarContadorAterrizajes() {
        try {
            semMutexAterrizaje.acquire();
        } catch (InterruptedException ex) {
            System.out.println(nombre() + ": Excepcion al adquirir semMutexAterrizaje");
        }
        cantidadAterrizajes = 0;
        semMutexAterrizaje.release();
    }
}
